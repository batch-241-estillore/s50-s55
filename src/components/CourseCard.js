import {useState, useEffect} from 'react'

import {Link} from 'react-router-dom';

import {Card, Button} from 'react-bootstrap';

function CourseCard({course}) {

  const {name, description, price, _id} = course;

  /*
    Syntax:
      const [getter, setter] = useState(intialGetterValue)
  */
  // const [count, setCount] = useState(0);
  //console.log(useState);
  // const [seats, setSeat] = useState(30);

  // const [isOpen, setIsOpen] = useState(true);

  /*function enroll() {
    //if(seats>0){
      setCount(count+1);
      setSeat(seats-1);  
    //}
    // else{
    //   alert("No more seats")
    //   document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
    // }
  }
*/
  /*useEffect(() => {
      if(seats === 0) {
        setIsOpen(false);
        document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
      }
  }, [seats])*/


  return (
    <Card style={{ width: '18rem' }} id="CourseCard">
    <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PhP {price}</Card.Text>
        {/*<Card.Text>Enrollees: {count}</Card.Text>*/}
        {/*<Button id={`btn-enroll-${id}`} className ="bg-primary" onClick={enroll}>Enroll</Button>      */}
        <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
    </Card.Body>
    </Card>
    );
}

export default CourseCard;