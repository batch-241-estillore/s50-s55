import { useState, useEffect, useContext } from 'react';

import { Form, Button } from 'react-bootstrap';

import UserContext from '../UserContext';

import { Navigate } from 'react-router-dom';

import Swal from 'sweetalert2';

export default function Login() {
    //Allows us to use the UserContext object and its properties to be used for user validation
    const {user, setUser} = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);


    //hook returns a function that lets you navigate to components
    //const navigate = useNavigate();

    function authenticate(e) {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })

        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(typeof data.access !== "undefined"){
                localStorage.setItem('token', data.access)
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })
            } else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Please, check your login details and try again."
                })
            }
        })
        
        //localStorage.setItem('email', email);

        //setUser({email: localStorage.getItem('email')});

        setEmail('');
        setPassword('');
        //navigate('/');
        

        //alert('Successfully logged in!')
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            //no need method get because it is already default
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {
        if(email && password){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email, password])

    return (
        (user.id !== null) ?
        <Navigate to="/courses"/>
        :
        <Form onSubmit={(e) => authenticate(e)}>
        <Form.Group controlId="userEmail">
        <h1>Login</h1>
        <Form.Label>Email address</Form.Label>
        <Form.Control 
        type="email" 
        placeholder="Enter email"
        value={email}
        onChange={e => setEmail(e.target.value)} 
        required
        />

        </Form.Group>

        <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control 
        type="password" 
        placeholder="Password"
        value={password}
        onChange={e => setPassword(e.target.value)}
        required
        />
        </Form.Group>


        { isActive ?
        <Button variant="success" type="submit" id="submitBtn">
        Login
        </Button>
        :
        <Button variant="danger" type="submit" id="submitBtn" disabled>
        Login
        </Button>
    }
    </Form>
    )

}