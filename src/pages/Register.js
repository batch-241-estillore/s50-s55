import { useState, useEffect, useContext } from 'react';

import {Navigate, useNavigate } from 'react-router-dom';

import UserContext from '../UserContext';

import { Form, Button } from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function Register() {

    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNumber, setMobileNumber] = useState('');
    const [isActive, setIsActive] = useState(false);
    const {user} = useContext(UserContext);
    const navigate = useNavigate();

    function registerUser(e) {

        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email
            })
        }).then(res => res.json()).then(data => {
            console.log(data);
            if(data){
                Swal.fire({
                    title: "Registration Failed",
                    icon: "error",
                    text: "Email already exists. Please use another email"
                })
            } else {

                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: 'POST',
                    headers:{
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNumber,
                        password: password1
                    })
                }).then(res => res.json()).then(data => {
                    console.log(data);
                    if(data){
                        Swal.fire({
                            title: "Registration Successful",
                            icon: "success",
                            text: "Welcome to Zuitt!"
                        })
                        navigate('/login')    
                    } else{
                        Swal.fire({
                            title: "Registration Failed",
                            icon: "error",
                            text: "Please check your details and try again."
                        })
                    }
                    

                })
                

            }
        })
        
        setEmail('');
        setPassword1('');
        setPassword2('');
        setFirstName('');
        setLastName('');
        setMobileNumber('');

        //alert('Thank you for registering!')
    }
    const numberlength = mobileNumber.length;
    useEffect(() => {
        if(email && password1 === password2 && password1 !== '' && numberlength == 11 && firstName && lastName){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email, password1, password2, firstName, lastName, mobileNumber])


    return (
        (user.id !== null)
        ?
        <Navigate to="/courses"/>
        :
        <Form onSubmit={(e) => registerUser(e)}>
            {/*firstName*/}
        <Form.Group controlId="userFirstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control 
        type="text" 
        placeholder="Enter first name"
        value={firstName}
        onChange={e => setFirstName(e.target.value)} 
        required
        />
        </Form.Group>

            {/*lastName*/}
        <Form.Group controlId="userLastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control 
        type="text" 
        placeholder="Enter last name"
        value={lastName}
        onChange={e => setLastName(e.target.value)} 
        required
        />
        </Form.Group>

        <Form.Group controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control 
        type="email" 
        placeholder="Enter email"
        value={email}
        onChange={e => setEmail(e.target.value)} 
        required
        />
        <Form.Text className="text-muted">
        We'll never share your email with anyone else.
        </Form.Text>
        </Form.Group>

            {/*Mobile number*/}
        <Form.Group controlId="mobileNumber">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control 
        type="text" 
        placeholder="Enter Mobile Number"
        value={mobileNumber}
        onChange={e => setMobileNumber(e.target.value)} 
        required
        />
        </Form.Group>

        <Form.Group controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control 
        type="password" 
        placeholder="Password"
        value={password1}
        onChange={e => setPassword1(e.target.value)}
        required
        />
        </Form.Group>

        <Form.Group controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control 
        type="password" 
        placeholder="Verify Password"
        value={password2}
        onChange={e => setPassword2(e.target.value)}
        required
        />
        </Form.Group>

        { isActive ?
        <Button variant="primary" type="submit" id="submitBtn">
        Submit
        </Button>
        :
        <Button variant="danger" type="submit" id="submitBtn">
        Submit
        </Button>
    }
    </Form>
    )

}